package com.jingwei;

import com.btlog.core.common.BootApp;
import com.btlog.core.common.boot.Apps;
import org.springframework.boot.SpringApplication;

/**
 * @author : 归无
 * @version : 1.0
 * @title
 * @description
 * @createTime : 2022/8/2 9:23 AM
 * @updateTime
 */
@BootApp(sysName = "jingwei-admin")
public class Main {
    public static void main(String[] args) {
        Apps.setProfileIfNotExists("dev");
        new SpringApplication(Main.class).run(args);
    }

}